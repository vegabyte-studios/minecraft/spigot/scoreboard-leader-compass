package studio.vegabyte.spigot.scoreboard_compass.items;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.CompassMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

import studio.vegabyte.spigot.scoreboard_compass.App;
import studio.vegabyte.spigot.scoreboard_compass.services.ConfigurationService;

/**
 * Utilities for the the Scoreboard Compass item
 */
public class CompassItem {
  // #region variables

  // prefab of the compass
  private ItemStack compassPrefab;

  // key denoting which scoreboard objective is being tracked
  private NamespacedKey keyObjective;

  // key denoting the player
  private NamespacedKey keyLastPlayerTracked;

  // #endregion

  // #region private - methods

  // update the lore
  private void setLore(ItemMeta meta, Score topOnlinePlayerScore) {

    meta.setLore(new ArrayList<String>() {
      {
        add("Use to track player.");
        add(String.format("Objective: %s", topOnlinePlayerScore.getObjective().getDisplayName()));
        add(String.format("Player: %s", topOnlinePlayerScore.getPlayer().getPlayer().getDisplayName()));
        add(String.format("Score: %d", topOnlinePlayerScore.getScore()));
      }
    });
  }

  // update the lore with only the objective
  private void setLore(ItemMeta meta, Objective objective) {

    meta.setLore(new ArrayList<String>() {
      {
        add("Use to track player.");
        add(String.format("Objective: %s", objective.getDisplayName()));
      }
    });
  }

  /**
   * Set the compass tracking. Update the position.
   */
  public void Track(ItemStack compass, Score topOnlinePlayerScore) {
    Player playerTarget = topOnlinePlayerScore.getPlayer().getPlayer();

    CompassMeta compassMeta = (CompassMeta) compass.getItemMeta();
    compassMeta.setLodestoneTracked(false);
    compassMeta.setLodestone(playerTarget.getLocation());
    setLore(compassMeta, topOnlinePlayerScore);

    PersistentDataContainer compassData = compassMeta.getPersistentDataContainer();

    // send message to hunted, if they are a new target of this compass
    String lastPlayerTracked = compassData.get(this.keyLastPlayerTracked, PersistentDataType.STRING);
    if (playerTarget.getUniqueId().toString() != lastPlayerTracked) {
      playerTarget.sendMessage("§e§l(!) §eYou are being hunted...");
    }

    compassData.set(this.keyLastPlayerTracked, PersistentDataType.STRING, playerTarget.getUniqueId().toString());
    compass.setItemMeta(compassMeta);
  }

  // instantiate a new compass
  public ItemStack Instantiate(Objective objectiveTrack) {
    ItemStack instance = new ItemStack(this.compassPrefab); // instantiate prefab

    // set meta - persistent data
    ItemMeta meta = instance.getItemMeta();
    PersistentDataContainer data = meta.getPersistentDataContainer();
    data.set(this.keyObjective, PersistentDataType.STRING, objectiveTrack.getName());
    setLore(meta, objectiveTrack);
    instance.setItemMeta(meta);

    return instance;
  }

  public ItemStack Instantiate(String objectiveName) {
    return this
        .Instantiate(this.app.getServer().getScoreboardManager().getMainScoreboard().getObjective(objectiveName));
  }

  // check if the item is a compass
  public boolean IsCompass(ItemStack item) {
    return item != null && item.getItemMeta().getPersistentDataContainer().has(keyObjective, PersistentDataType.STRING);
  }

  /**
   * Get the tracked objective name. ie, get the name of the objective which the
   * compass tracks.
   */
  public String GetObjectiveName(ItemStack compass) {
    return compass.getItemMeta().getPersistentDataContainer().get(this.keyObjective, PersistentDataType.STRING);
  }

  // #endregion

  // #region private - compass

  // create the compass prefab
  private void createCompassPrefab() {
    ItemStack prefab = new ItemStack(Material.COMPASS, 1);

    // set meta
    ItemMeta meta = prefab.getItemMeta();

    // set meta - display
    meta.setDisplayName("Scoreboard Compass");

    prefab.setItemMeta(meta);

    this.compassPrefab = prefab;
  }

  private void createRecipe() {
    NamespacedKey key = new NamespacedKey(this.app, "scoreboard_compass");

    String objectiveName = this.configurationService.CraftableObjective();

    ItemStack compass = Instantiate(objectiveName);

    // Create our custom recipe variable
    ShapedRecipe recipe = new ShapedRecipe(key, compass);
    recipe.shape(" G ", "GLG", " G ");
    recipe.setIngredient('G', Material.GOLD_INGOT);
    recipe.setIngredient('L', Material.LAPIS_LAZULI);

    // Finally, add the recipe to the bukkit recipes
    Bukkit.addRecipe(recipe);
  }

  // #endregion

  // #region Lifecycle

  private App app;
  private ConfigurationService configurationService;

  public CompassItem(App app, ConfigurationService configurationService) {
    this.app = app;
    this.configurationService = configurationService;

    // initialize keys
    keyObjective = new NamespacedKey(app, "objective");
    keyLastPlayerTracked = new NamespacedKey(app, "lastPlayerTracked");

    createCompassPrefab();

    // create the recipe
    if (this.configurationService.IsCraftable()) {
      this.createRecipe();
    }
  }

  // #endregion
}

package studio.vegabyte.spigot.scoreboard_compass.managers;

import studio.vegabyte.spigot.scoreboard_compass.App;
import studio.vegabyte.spigot.scoreboard_compass.items.CompassItem;
import studio.vegabyte.spigot.scoreboard_compass.services.ScoreService;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scoreboard.Score;

// manages compasses
public class CompassManager implements Listener {

  // #region handlers

  @EventHandler
  private void onPlayerInteract(PlayerInteractEvent event) {
    if (event.getItem() == null)
      return;

    if (event.getAction() == Action.RIGHT_CLICK_AIR
        || (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() != Material.LODESTONE)) {

      ItemStack item = event.getItem();
      if (this.compassItem.IsCompass(item)) {

        // determine top scoring player
        String objective = this.compassItem.GetObjectiveName(item);
        Score playerTopScore = this.scoreService.GetTopOnlinePlayerScore(objective, event.getPlayer());

        // update the compass meta data with the players top score
        if (playerTopScore != null) {
          this.compassItem.Track(item, playerTopScore);

          event.getPlayer().sendMessage(
              String.format("§e§l(!) §eTracking '%s'...", playerTopScore.getPlayer().getPlayer().getDisplayName()));
        } else {
          event.getPlayer().sendMessage("§e§l(!) §eNo valid player to track.");
        }
      }
    }
  }

  // #endregion

  // #region lifecycle

  private App app;
  private ScoreService scoreService;
  private CompassItem compassItem;

  public CompassManager(App app, CompassItem compassItem, ScoreService scoreService) {
    this.app = app;
    this.scoreService = scoreService;
    this.compassItem = compassItem;

    this.app.getServer().getPluginManager().registerEvents(this, this.app); // register events
  }

  // #endregion
}
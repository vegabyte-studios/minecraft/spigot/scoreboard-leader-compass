package studio.vegabyte.spigot.scoreboard_compass.services;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import studio.vegabyte.spigot.scoreboard_compass.App;

// manages scores which are tracked
public class ScoreService {

  // #region methods - public

  // get score of the tracked objective of players who are online
  // returns null if no valid players.
  public Score GetTopOnlinePlayerScore(String objectiveName, Player playerExcluded) {

    Objective objective = this.scoreboard.getObjective(objectiveName);

    // validate existence
    if (objective == null) {
      throw new IllegalArgumentException(String.format("No objective '%s'.", objectiveName));
    }

    Score forTopScore = null;
    for (String entry : this.scoreboard.getEntries()) {
      Score iScore = objective.getScore(entry);

      // determine if player is valid target
      boolean isPlayerValid = iScore.getPlayer() != null && iScore.getPlayer().isOnline()
          && iScore.getPlayer().getPlayer() != playerExcluded;

      if (isPlayerValid && (forTopScore == null || iScore.getScore() > forTopScore.getScore())) {
        forTopScore = iScore;
      }
    }

    return forTopScore;
  }

  // #endregion

  // #region lifecycle

  private final App app;
  private Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();

  public ScoreService(App app) {
    this.app = app;
  }

  // #endregion
}

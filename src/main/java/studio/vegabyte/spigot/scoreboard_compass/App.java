package studio.vegabyte.spigot.scoreboard_compass;

import org.bukkit.plugin.java.JavaPlugin;

import studio.vegabyte.spigot.scoreboard_compass.items.CompassItem;
import studio.vegabyte.spigot.scoreboard_compass.services.ConfigurationService;
import studio.vegabyte.spigot.scoreboard_compass.services.ScoreService;
import studio.vegabyte.spigot.scoreboard_compass.commands.GiveCommands;
import studio.vegabyte.spigot.scoreboard_compass.managers.CompassManager;

public class App extends JavaPlugin {

  // services
  private ConfigurationService configurationService;

  // managers
  private CompassManager compassManager;
  private ScoreService scoreService;

  // commands
  private GiveCommands giveCommands;

  // items
  private CompassItem compassItem;

  @Override
  public void onEnable() {

    this.configurationService = new ConfigurationService(this);
    this.compassItem = new CompassItem(this, this.configurationService);
    this.scoreService = new ScoreService(this);
    this.compassManager = new CompassManager(this, this.compassItem, this.scoreService);
    this.giveCommands = new GiveCommands(this, this.compassItem);
  }

  @Override
  public void onDisable() {
    getLogger().info(this.getName() + " Disabled");
  }
}